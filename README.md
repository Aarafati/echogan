
# ECHOGAN: A Fully Convolutional Neural Network for 4-chamber EchoCardiogram Segmentation complemented by a discriminator 

A Keras implementation of the model in the paper [Generalizable Fully Automated Multi-Label Segmentation of 4-Chamber View Echocardiograms based on Deep Convolutional Adversarial Networks]. 


The model has been trained on a private dataset of 946 4-chamber view echocardiograms from Loma Linda Hospital, and this version is the test script for the public CAMUS dataset (https://www.creatis.insa-lyon.fr/Challenge/camus/databases.html).

## Requirements
The code is tested on CentOS Linux release 7.2.1511 (Core) with the following components:

### Software
Latest version of the following softwares and packages are used to test the baseline.

* Python 3.5
* Keras version : 2.3.1 using TensorFlow backend
* Pydicom
* Opencv-python
* Regex
* NumPy
* h5py
* PIL
* Pandas


To evaluate the model on the training dataset of CAMUS data, execute the following in the command line:


```bash
# Create sample images and their corresponding masks
$ python test_camus.py
```



