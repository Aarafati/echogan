import numpy as np
from model import GAN, discriminator_image, generator
import util_camus as utils
import os
import scipy.io as sio
import tensorflow as tf
from keras.utils import to_categorical

physical_devices = tf.config.experimental.list_physical_devices('GPU')
assert len(physical_devices) > 0, "Not enough GPU hardware devices available"
tf.config.experimental.set_memory_growth(physical_devices[0], True)
# os.environ['CUDA_VISIBLE_DEVICES'] = 0  # FLAGS.gpu_index
"""###############################################################################################################
                                                    CONSTANTS
#################################################################################################################"""
# training settings
n_rounds = 150
batch_size = 1
n_filters_d = 16
n_filters_g = 16
init_lr = 2e-5
NUM_CLASSES = 5
img_size = (256, 256)
ratio_gan2seg = 10

"""###############################################################################################################
                                                    NETWORK
#################################################################################################################"""
loss = 'categorical_crossentropy'
activation = 'softmax'
alpha_recip = 1. / ratio_gan2seg
rounds_for_evaluation = range(n_rounds)

# discriminator type
discriminator = 'image'

img_out_dir = "./segmentation_results_{}_{}".format(discriminator, ratio_gan2seg)
model_out_dir = "./model_{}_{}".format(discriminator, ratio_gan2seg)

if not os.path.isdir(img_out_dir):
    os.makedirs(img_out_dir)
if not os.path.isdir(model_out_dir):
    os.makedirs(model_out_dir)

# create networks
g = generator(img_size, n_filters_g)

d, d_out_shape = discriminator_image(img_size, n_filters_d, init_lr)

utils.make_trainable(d, False)

gan = GAN(g, d, img_size, n_filters_g, n_filters_d, alpha_recip, init_lr)

g.summary()
d.summary()
gan.summary()

"""###############################################################################################################
                                                    EVALUATION
#################################################################################################################"""
# set test dataset
test_dir = './all_CAMUS_4ch_256.npy'
test_imgs, test_masks = utils.get_imgs(test_dir)

# evaluate on test images
g.load_weights("./gLR-5_16_image_1.h5")
generated = g.predict(np.expand_dims(test_imgs, 3), batch_size=batch_size)
all_in_mask = np.argmax(generated, axis=3)
binarys_in_mask = to_categorical(all_in_mask, 5)
chambers_in_mask, generated_in_mask = utils.pixel_values_in_mask(test_masks, generated)

print(chambers_in_mask.shape, generated_in_mask.shape, binarys_in_mask.shape, np.max(binarys_in_mask),
      np.max(generated_in_mask), 'predice')
sio.savemat('Echo_CAMUS_ordered.mat', {'auto_mask': binarys_in_mask, 'gt_mask': test_masks})

dice_la = np.zeros(900)
dice_lv = np.zeros(900)

for ii in range(900):  # Very slow!
    dice_lv[ii] = np.sum(test_masks[ii, :, :, 1] * (binarys_in_mask[ii, :, :, 1])) * 2.0 / (
            np.sum(test_masks[ii, :, :, 1]) + np.sum(binarys_in_mask[ii, :, :, 1]))

    dice_la[ii] = np.sum(test_masks[ii, :, :, 3] * (binarys_in_mask[ii, :, :, 3])) * 2.0 / (
            np.sum(test_masks[ii, :, :, 3]) + np.sum(binarys_in_mask[ii, :, :, 3]))


sio.savemat('Echo_CAMUS_result_ordered.mat', {'gt': test_masks, 'auto': binarys_in_mask, 'test_imgs': test_imgs,
            'all_chambers': all_in_mask.astype(np.uint8), 'dice_la': dice_la, "dice_lv": dice_lv})
