import SimpleITK as sitk
import numpy as np
import scipy.misc as sci
import matplotlib as mpl

mpl.use('Agg')
all_4ch_images2 = np.empty([4, 256, 256, 450])

for i in range(450):
    num = '000' + str(i + 1)
    num = num[-3:]
    filename_ES_img = '/home/exx/Downloads/training/patient0' + num + '/patient0' + num + '_4CH_ES.mhd'
    filename_ED_img = '/home/exx/Downloads/training/patient0' + num + '/patient0' + num + '_4CH_ED.mhd'
    filename_ED_lbl = '/home/exx/Downloads/training/patient0' + num + '/patient0' + num + '_4CH_ED_gt.mhd'
    filename_ES_lbl = '/home/exx/Downloads/training/patient0' + num + '/patient0' + num + '_4CH_ES_gt.mhd'
    itkimage_ED_img = sitk.GetArrayFromImage(sitk.ReadImage(filename_ED_img))
    itkimage_ES_img = sitk.GetArrayFromImage(sitk.ReadImage(filename_ES_img))
    itkimage_ED_lbl = sitk.GetArrayFromImage(sitk.ReadImage(filename_ED_lbl))
    itkimage_ES_lbl = sitk.GetArrayFromImage(sitk.ReadImage(filename_ES_lbl))
    print(i)
    all_4ch_images2[0, :, :, i] = sci.imresize(itkimage_ED_img[0], (256, 256), 'nearest')
    all_4ch_images2[1, :, :, i] = sci.imresize(itkimage_ED_lbl[0], (256, 256), 'nearest')
    all_4ch_images2[2, :, :, i] = sci.imresize(itkimage_ES_img[0], (256, 256), 'nearest')
    all_4ch_images2[3, :, :, i] = sci.imresize(itkimage_ES_lbl[0], (256, 256), 'nearest')

np.save('all_CAMUS_4ch_256.npy', all_4ch_images2)
