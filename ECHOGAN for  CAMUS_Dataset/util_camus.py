import sys
from keras.preprocessing.image import Iterator
from keras.utils import to_categorical
import numpy as np
import pickle


def make_trainable(net, val):
    net.trainable = val
    for lay in net.layers:
        lay.trainable = val


def discriminator_shape(n, d_out_shape):
    if len(d_out_shape) == 1:  # image gan
        return n, d_out_shape[0]
    elif len(d_out_shape) == 3:  # pixel, patch gan
        return n, d_out_shape[0], d_out_shape[1], d_out_shape[2]
    return None


def input2gan(real_img_patches, real_vessel_patches, d_out_shape):
    g_x_batch = [real_img_patches, real_vessel_patches]
    # set 1 to all labels (real : 1, fake : 0)
    g_y_batch = np.ones(discriminator_shape(real_vessel_patches.shape[0], d_out_shape))
    return g_x_batch, g_y_batch


def print_metrics(itr, **kargs):
    print("*** Round {}  ====> ".format(itr))
    for name, value in kargs.items():
        print("{} : {}, ".format(name, value)),
    print("")
    sys.stdout.flush()


class TrainBatchFetcher(Iterator):
    """
    fetch batch of original images and vessel images
    """

    def __init__(self, train_imgs, train_vessels, batch_size):
        self.train_imgs = train_imgs
        self.train_vessels = train_vessels
        self.n_train_imgs = self.train_imgs.shape[0]
        self.batch_size = batch_size

    def next(self):
        indices = list(np.random.choice(self.n_train_imgs, self.batch_size))
        return self.train_imgs[indices, :, :], self.train_vessels[indices, :, :, :]


def save_obj(obj, name):
    with open(name, 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)


def load_obj(name):
    with open(name, 'rb') as f:
        return pickle.load(f)


def get_imgs(target_dir):

    # load images
    x = np.load(target_dir)

    echo_imgs = np.transpose(np.fliplr(np.concatenate((x[0, :, :, :], x[2, :, :, :]), axis=2)), (2, 0, 1))
    gt_imgs = np.transpose(np.fliplr(np.concatenate((x[1, :, :, :], x[3, :, :, :]), axis=2)), (2, 0, 1))

    gt_imgs = to_categorical(gt_imgs, 4)

    # z score with mean, std of each image
    n_all_imgs = echo_imgs.shape[0]
    for index in range(n_all_imgs):
        mean = np.mean(echo_imgs[index, ...][echo_imgs[index, ...] > 40.0], axis=0)
        std = np.std(echo_imgs[index, ...][echo_imgs[index, ...] > 40.0], axis=0)
        # DEBUG:
        # assert len(mean)==1 and len(std)==1
        # print('mean,std',len(mean),len(std))
        echo_imgs[index, ...] = (echo_imgs[index, ...] - mean) / std

    return echo_imgs, gt_imgs


def pixel_values_in_mask(gt_masks, pred_masks):
    print(np.max(pred_masks), 'max', np.min(pred_masks), 'min')
    assert np.max(pred_masks) <= 1.0 and np.min(pred_masks) >= 0.0
    assert np.max(gt_masks) == 1.0 and np.min(gt_masks) == 0.0

    return gt_masks, pred_masks

