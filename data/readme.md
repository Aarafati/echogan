### This directory includes data used for training and validation of ECHOGAN, published : 

Arafati A, Morisawa D, Avendi MR, et al. Generalizable fully automated multi-label segmentation of four-chamber view echocardiograms based on deep convolutional adversarial networks. J R Soc Interface. 2020;17(169):20200267. doi:10.1098/rsif.2020.0267


### Folders are organized as described below: 

* /training :	4-chamber long axis view TTE images and manual annotations of subjects used for training of the model. 
* /test: 	4-chamber long axis view TTE images and manual annotations of subjects used for testing the model's performance.
* /testing_ESED: 	this folder includes end-systolic and end-diastolic frames for subjects in the testing cohort. These frames may be used to calculate end-systolic and end-diastolic volumes.